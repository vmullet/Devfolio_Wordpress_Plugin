<?php


class devfolio_language {


    private $id;
    private $name;
    private $byte;
    private $percent;

    /**
     * devfolio_language constructor.
     * @param $id
     * @param $name
     * @param $byte
     * @param $percent
     */
    public function __construct($id, $name, $byte, $percent)
    {
        $this->id = $id;
        $this->name = $name;
        $this->byte = $byte;
        $this->percent = $percent;
    }

    public function __get($prop)
    {
        return $this->$prop;
    }

    public function __isset($prop) : bool
    {
        return isset($this->$prop);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getByte()
    {
        return $this->byte;
    }

    /**
     * @param mixed $byte
     */
    public function setByte($byte)
    {
        $this->byte = $byte;
    }

    /**
     * @return mixed
     */
    public function getPercent()
    {
        if ($this->percent==0)
            return 1;
        else
            return $this->percent;
    }

    /**
     * @param mixed $percent
     */
    public function setPercent($percent)
    {
        $this->percent = $percent;
    }






}



?>