<?php
/**
 * Created by PhpStorm.
 * User: Valentin
 * Date: 15/01/2017
 * Time: 17:16
 */

abstract class devfolio_baseProfile {

    protected $_username;
    protected $_truename;
    protected $_avatar_url;
    protected $_site;
    protected $_location;
    protected $_creation_date;

    protected function __construct($username,$truename,$avatar_url,$site,$location,$creation_date) {

        $this->_username = $username;
        $this->_truename = $truename;
        $this->_avatar_url = $avatar_url;
        $this->_site = $site;
        $this-> _location = $location;
        $this-> _creation_date = $creation_date;

    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->_username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->_username = $username;
    }

    /**
     * @return mixed
     */
    public function getTruename()
    {
        return $this->_truename;
    }

    /**
     * @param mixed $truename
     */
    public function setTruename($truename)
    {
        $this->_truename = $truename;
    }

    /**
     * @return mixed
     */
    public function getAvatarUrl()
    {
        return $this->_avatar_url;
    }

    /**
     * @param mixed $avatar_url
     */
    public function setAvatarUrl($avatar_url)
    {
        $this->_avatar_url = $avatar_url;
    }

    /**
     * @return mixed
     */
    public function getSite()
    {
        return $this->_site;
    }

    /**
     * @param mixed $site
     */
    public function setSite($site)
    {
        $this->_site = $site;
    }

    /**
     * @return mixed
     */
    public function getLocation()
    {
        return $this->_location;
    }

    /**
     * @param mixed $location
     */
    public function setLocation($location)
    {
        $this->_location = $location;
    }

    /**
     * @return mixed
     */
    public function getCreationDate()
    {
        return $this->_creation_date;
    }

    /**
     * @param mixed $creation_date
     */
    public function setCreationDate($creation_date)
    {
        $this->_creation_date = $creation_date;
    }




}


?>