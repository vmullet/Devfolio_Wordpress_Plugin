<?php


class devfolio_commit {


    private $id;
    private $message;
    private $author;
    private $url;
    private $date;

    /**
     * devfolio_commit constructor.
     * @param $id
     * @param $message
     * @param $author
     * @param $url
     * @param $date
     */
    public function __construct($id, $message, $author, $date, $url)
    {
        $this->id = $id;
        $this->message = $message;
        $this->author = $author;
        $this->date = $date;
        $this->url = $url;

    }

    public function __get($prop)
    {
        return $this->$prop;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }





}



?>