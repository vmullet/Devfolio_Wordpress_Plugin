<?php

require_once('devfolio_commit.class.php');
require_once('devfolio_language.class.php');

class devfolio_baseProject {

    private $id;
    private $name;
    private $fullname;
    private $description;
    private $html_url;
    private $private;
    private $created_at;
    private $size;
    private $nb_forks;
    private $default_branch;
    private $download_url;
    private $readme;

    private $languages;

    private $commits;
	
	private $license;
	private $license_url;

    /**
     * devfolio_baseProject constructor.
     * @param $id
     * @param $name
     * @param $fullname
     * @param $description
     * @param $html_url
     * @param $private
     * @param $created_at
     * @param $size
     * @param $nb_forks
     * @param $default_branch
     * @param $download_url
     * @param $readme
     * @param $languages
     * @param $commits
     */
    public function __construct($id, $name, $fullname, $description, $html_url, $private, $created_at, $size, $nb_forks, $default_branch, $download_url, $readme, $languages, $commits, $license, $license_url)
    {
        $this->id = $id;
        $this->name = $name;
        $this->fullname = $fullname;
        $this->description = $description;
        $this->html_url = $html_url;
        $this->private = $private;
        $this->created_at = $created_at;
        $this->size = $size;
        $this->nb_forks = $nb_forks;
        $this->default_branch = $default_branch;
        $this->download_url = $download_url;
        $this->readme = $readme;
        $this->languages = $languages;
        $this->commits = $commits;
		$this->license = $license;
		$this->license_url = $license_url;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * @param mixed $fullname
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getHtmlUrl()
    {
        return $this->html_url;
    }

    /**
     * @param mixed $html_url
     */
    public function setHtmlUrl($html_url)
    {
        $this->html_url = $html_url;
    }

    /**
     * @return mixed
     */
    public function getPrivate()
    {
        return $this->private;
    }

    /**
     * @param mixed $private
     */
    public function setPrivate($private)
    {
        $this->private = $private;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param mixed $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * @return mixed
     */
    public function getNbForks()
    {
        return $this->nb_forks;
    }

    /**
     * @param mixed $nb_forks
     */
    public function setNbForks($nb_forks)
    {
        $this->nb_forks = $nb_forks;
    }

    /**
     * @return mixed
     */
    public function getDefaultBranch()
    {
        return $this->default_branch;
    }

    /**
     * @param mixed $default_branch
     */
    public function setDefaultBranch($default_branch)
    {
        $this->default_branch = $default_branch;
    }

    /**
     * @return mixed
     */
    public function getDownloadUrl()
    {
        return $this->download_url;
    }

    /**
     * @param mixed $download_url
     */
    public function setDownloadUrl($download_url)
    {
        $this->download_url = $download_url;
    }

    /**
     * @return mixed
     */
    public function getReadme()
    {
            return $this->readme;
    }

    /**
     * @param mixed $readme
     */
    public function setReadme($readme)
    {
        $this->readme = $readme;
    }

    /**
     * @return devfolio_language[]
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * @param devfolio_language[] $languages
     */
    public function setLanguages($languages)
    {
        $this->languages = $languages;
    }

    /**
     * @return devfolio_commit[]
     */
    public function getCommits()
    {
        return $this->commits;
    }

    /**
     * @param devfolio_commit[] $commits
     */
    public function setCommits($commits)
    {
        $this->commits = $commits;
    }

/**
     * @return mixed
     */
    public function getLicense()
    {
        return $this->license;
    }

    /**
     * @param mixed $commits
     */
    public function setLicense($license)
    {
        $this->license = $license;
    }
	
	/**
     * @return mixed
     */
    public function getLicenseUrl()
    {
        return $this->license_url;
    }

    /**
     * @param mixed $license_url
     */
    public function setLicenseUrl($license_url)
    {
        $this->license_url = $license_url;
    }


}


?>