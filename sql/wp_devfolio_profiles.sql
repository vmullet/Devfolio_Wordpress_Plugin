CREATE TABLE IF NOT EXISTS `wp_devfolio_profiles` (
  `id` int(11) NOT NULL,
  `username` varchar(25) NOT NULL,
  `truename` varchar(30) NOT NULL,
  `avatar_url` text NOT NULL,
  `description` text,
  `site` text,
  `email` varchar(40) DEFAULT NULL,
  `location` varchar(25) DEFAULT NULL,
  `creation_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
