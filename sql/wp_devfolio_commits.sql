CREATE TABLE IF NOT EXISTS `wp_devfolio_commits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` varchar(200) NOT NULL,
  `author` varchar(50) NOT NULL,
  `date` datetime NOT NULL,
  `html_url` text NOT NULL,
  `project_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

ALTER TABLE `wp_devfolio_commits`
  ADD CONSTRAINT `wp_devfolio_commits_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `wp_devfolio_github_projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
