CREATE TABLE IF NOT EXISTS `wp_devfolio_github_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(75) NOT NULL,
  `fullname` varchar(200) NOT NULL,
  `description` text,
  `html_url` varchar(200) NOT NULL,
  `private` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `size` int(11) NOT NULL,
  `nbforks` int(11) NOT NULL,
  `default_branch` varchar(25) NOT NULL,
  `has_license` tinyint(4) NOT NULL,
  `download_url` varchar(150) NOT NULL,
  `readme` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
