CREATE TABLE IF NOT EXISTS `wp_devfolio_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `path` varchar(100) NOT NULL,
  `sha` varchar(50) NOT NULL,
  `type` varchar(15) NOT NULL,
  `content` text NOT NULL,
  `size` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

ALTER TABLE `wp_devfolio_content`
  ADD CONSTRAINT `wp_content_project` FOREIGN KEY (`project_id`) REFERENCES `wp_devfolio_github_projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

