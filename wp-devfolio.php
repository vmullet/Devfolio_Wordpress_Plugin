<?php

/**
 * Plugin Name: DevFolio
 * Plugin URI: http://lostarchives.fr
 * Author: Valentin Mullet
 * Author URI: http://lostarchives.fr
 * Version: 1.0
 * Description: A plugin to create a gallery (portfolio) of your development projects from gitHub
 */

include_once('menu/devfolio-menu-register.php');
include_once('menu/devfolio-menu-displayer.php');
require_once('managers/devfolio_pluginManager.class.php');
require_once('managers/devfolio_cacheManager.class.php');
require_once('managers/devfolio_optionManager.class.php');
require_once('managers/devfolio_connectionManager.class.php');
require_once('managers/devfolio_projectManager.class.php');


function devfolio_activate() {
    devfolio_pluginManager::Instance()->create_options();
    devfolio_pluginManager::Instance()->create_tables();
    devfolio_pluginManager::Instance()->create_files();
    devfolio_pluginManager::Instance()->create_pages();
    
}

function devfolio_deactivate() {
    devfolio_pluginManager::Instance()->remove_options();
    devfolio_pluginManager::Instance()->remove_tables();
    devfolio_pluginManager::Instance()->remove_pages();
    devfolio_pluginManager::Instance()->remove_files();
    
}


register_activation_hook(__FILE__, 'devfolio_activate');
register_deactivation_hook(__FILE__, 'devfolio_deactivate');

add_action('admin_menu', 'devfolio_add_menu');
add_action('admin_init', 'devfolio_init_register_settings');


?>