<?php

include_once('devfolio_connectionManager.class.php');
include_once('devfolio_queryManager.class.php');

/**
 * Class devfolio_cacheManager to cache gitHub data (repositories and profile)
 */
class devfolio_cacheManager
{

    private $instance = null;

    private $nb_license;
    private $nb_readme;


    private function __construct()
    {
        $this->nb_license = 0;
        $this-> nb_readme = 0;
    }

    public static function Instance()
    {

        if (!isset($instance)) {

            $instance = new devfolio_cacheManager();

        }

        return $instance;

    }


    /**
     * Function to get the gitHub profile information
     */
    public function cache_profiles()
    {

        $github_response = devfolio_queryManager::Instance()->getBody('https://api.github.com/users/' . devfolio_optionManager::Instance()->get_github_username() . '?access_token=' . devfolio_optionManager::Instance()->get_github_token());

        devfolio_connectionManager::Instance()->insert('wp_devfolio_profiles', array(

            'id' => '',
            'username' => $github_response['login'],
            'truename' => $github_response['name'],
            'avatar_url' => $github_response['avatar_url'],
            'description' => $github_response['bio'],
            'site' => $github_response['blog'],
            'email' => $github_response['email'],
            'location' => $github_response['location'],
            'nb_followers' => $github_response['followers'],
            'nb_following' => $github_response['following'],
            'nb_repos_total' => $github_response['public_repos'],
            'nb_repos_no_license' => $github_response['public_repos'] - $this->nb_license,
            'nb_repos_no_readme' => $github_response['public_repos'] - $this->nb_readme,
            'prefered_license' => '',
            'creation_date' => $github_response['created_at']


        ));


    }

    /**
     * Function to get and cache repository data such as repository metadata,languages,commits,content
     */
    public function cache_gitHubProjects()
    {

        $github_projects = devfolio_queryManager::Instance()->getBody('https://api.github.com/users/' . devfolio_optionManager::Instance()->get_github_username() . '/repos?access_token=' . devfolio_optionManager::Instance()->get_github_token());
        $this->nb_license = 0;
        $this->nb_readme = 0;

        foreach ($github_projects as $github_project) {

            // To get readme html version
            $args = array(
                'headers' => array(
                    'Accept' => 'application/vnd.github.VERSION.html'
                )
            );

            // Get Repository readme content
            $readme_response = devfolio_queryManager::Instance()->get('https://api.github.com/repos/' . devfolio_optionManager::Instance()->get_github_username() . '/' . $github_project['name'] . '/readme?access_token=' . devfolio_optionManager::Instance()->get_github_token(), $args, false);

            $has_readme = false;
            if ($readme_response!='') {
                $has_readme = true;
                $this->nb_readme++;
            }

            // Get Repository license
            $license_response = devfolio_queryManager::Instance()->getBody('https://api.github.com/repos/' . devfolio_optionManager::Instance()->get_github_username() . '/' . $github_project['name'] . '/license?access_token=' . devfolio_optionManager::Instance()->get_github_token());

            $has_license = false;
            if (isset($license_response['license'])) {
                $has_license = true;
                $this->nb_license++;
            }

            //Insert project data
            devfolio_connectionManager::Instance()->insert('wp_devfolio_github_projects', array(

                    'id' => $github_project['id'],
                    'name' => $github_project['name'],
                    'fullname' => $github_project['full_name'],
                    'description' => $github_project['description'],
                    'html_url' => $github_project['html_url'],
                    'private' => $github_project['private'],
                    'created_at' => $github_project['created_at'],
                    'size' => $github_project['size'],
                    'nbforks' => $github_project['forks'],
                    'default_branch' => $github_project['default_branch'],
                    'has_readme' => $has_readme,
                    'has_license' => $has_license,
                    'download_url' => 'https://github.com/' . devfolio_optionManager::Instance()->get_github_username() . '/' . $github_project['name'] . '/archive/master.zip',
                    'readme' => $readme_response


                )


            );

            // Insert license data
            if ($has_license) {
                devfolio_connectionManager::Instance()->insert('wp_devfolio_license',array(
                    'id' => '',
                    'name' => $license_response['license']['name'],
                    'url' => $license_response['license']['url'],
                    'project_id' => $github_project['id']
                ));
            }
            else  {
                devfolio_connectionManager::Instance()->insert('wp_devfolio_license',array(
                    'id' => '',
                    'name' => 'No License',
                    'url' => '#',
                    'project_id' => $github_project['id']
                ));
            }

            ///////////////////////////////////////////////////////////////// Project Languages //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $languages = devfolio_queryManager::Instance()->getBody('https://api.github.com/repos/' . devfolio_optionManager::Instance()->get_github_username() . '/' . $github_project['name'] . '/languages?access_token=' . devfolio_optionManager::Instance()->get_github_username());

            $sum_byte = 0;

            foreach ($languages as $language => $byte) {

                $sum_byte += $byte;

            };

            foreach ($languages as $language => $byte) {

                $percent = $byte / $sum_byte;

                devfolio_connectionManager::Instance()->insert('wp_devfolio_languages', array(

                    'id' => '',
                    'name' => $language,
                    'nb_bytes' => $byte,
                    'percent' => round($percent * 100),
                    'project_id' => $github_project['id']


                ));

            }

            ///////////////////////////////////////////////////////////////////// Project commits ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $commits = devfolio_queryManager::Instance()->getBody('https://api.github.com/repos/' . devfolio_optionManager::Instance()->get_github_username() . '/' . $github_project['name'] . '/commits?author=' . devfolio_optionManager::Instance()->get_github_username() . '&access_token=' . devfolio_optionManager::Instance()->get_github_token());

            foreach ($commits as $commit) {

                devfolio_connectionManager::Instance()->insert('wp_devfolio_commits', array(

                    'id' => '',
                    'message' => $commit['commit']['message'],
                    'author' => $commit['commit']['committer']['name'],
                    'html_url' => $commit['html_url'],
                    'date' => $commit['commit']['committer']['date'],
                    'project_id' => $github_project['id']


                ));

            }

            if (strpos($github_project['name'], 'ansible') === false && strpos($github_project['name'], 'gitignore') === false && strpos($github_project['name'], 'sinon') === false) {
                $this->cache_project_content($github_project['name'], $github_project['id']);
            }


        }


    }



    /**
     * Function to cache the repository content
     * @param $project_name string
     * @param $project_id int
     */
    private function cache_project_content($project_name, $project_id)
    {

        $path = 'root';

        $contents = devfolio_queryManager::Instance()->getBody('https://api.github.com/repos/' . devfolio_optionManager::Instance()->get_github_username() . '/' . $project_name . '/contents?path=&access_token=' . devfolio_optionManager::Instance()->get_github_token());

        $file_args = array(
            'headers' => array(
                'Accept' => 'application/vnd.github.VERSION.raw'
            )
        );

        foreach ($contents as $content) {

            $file_content = '';

            // Get file content
            if ($content['type'] == 'file' && !strpos('.jpg', $content['name'])) {
                $file_content = devfolio_queryManager::Instance()->get($content['git_url'] . '?access_token=' . devfolio_optionManager::Instance()->get_github_token(), $file_args, false);
            }

            devfolio_connectionManager::Instance()->insert('wp_devfolio_content', array(

                'id' => '',
                'name' => $content['name'],
                'path' => dirname($path . '/' . $content['path']),
                'sha' => $content['sha'],
                'type' => $content['type'],
                'size' => $content['size'],
                'content' => $file_content,
                'project_id' => $project_id


            ));

            // If content is a directory
            if ($content['type'] == 'dir') {

                // Get its content recursively
                $trees = devfolio_queryManager::Instance()->getBody('https://api.github.com/repos/' . devfolio_optionManager::Instance()->get_github_username() . '/' . $project_name . '/git/trees/' . $content['sha'] . '?recursive=1&access_token=' . devfolio_optionManager::Instance()->get_github_token())['tree'];

                foreach ($trees as $tree) {

                    $file_content = '';
                    $type = '';
                    $size = 0;

                    // if it is a file
                    if ($tree['type'] == 'blob') {
                        $type = 'file';
                        $size = $tree['size'];
                        if (!strpos('.jpg', $tree['path'])) {
                            $file_content = devfolio_queryManager::Instance()->get($tree['url'] . '?access_token=' . devfolio_optionManager::Instance()->get_github_token(), $file_args, false);
                        }
                    } else {
                        $type = 'dir';
                    }

                    // Insert every folder/file
                    devfolio_connectionManager::Instance()->insert('wp_devfolio_content', array(

                        'id' => '',
                        'name' => basename($tree['path']),
                        'path' => dirname($path . '/' . $content['path'] . '/' . $tree['path']),
                        'sha' => $tree['sha'],
                        'type' => $type,
                        'size' => $size,
                        'content' => utf8_encode($file_content),
                        'project_id' => $project_id


                    ));

                }

            }


        }


    }


    /**
     * Function to empty the database cache
     */
    public function emptyCache()
    {

        devfolio_connectionManager::Instance()->query('Delete from wp_devfolio_languages');
        devfolio_connectionManager::Instance()->query('Delete from wp_devfolio_content');
        devfolio_connectionManager::Instance()->query('Delete from wp_devfolio_commits');
        devfolio_connectionManager::Instance()->query('Delete from wp_devfolio_profiles');
        devfolio_connectionManager::Instance()->query('Delete from wp_devfolio_license');
        devfolio_connectionManager::Instance()->query('Delete from wp_devfolio_github_projects');


    }


}


?>