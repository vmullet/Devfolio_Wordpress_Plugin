<?php

require_once('devfolio_connectionManager.class.php');
require_once(dirname(__FILE__, 2) . '/model/devfolio_gitHub_profile.class.php');
require_once(dirname(__FILE__, 2) . '/model/devfolio_baseProject.class.php');
require_once(dirname(__FILE__, 2) . '/model/devfolio_language.class.php');
require_once(dirname(__FILE__, 2) . '/model/devfolio_commit.class.php');


class devfolio_projectManager
{

    private static $instance = null;
    private $options;

    private function __construct()
    {
        $this->options = get_option('devfolio_options');
    }

    public static function Instance()
    {

        if (!isset(self::$instance)) {

            self::$instance = new devfolio_projectManager();

        }

        return self::$instance;

    }

    /**
     * @return devfolio_gitHub_profile
     */
    public function LoadGitHubProfile()
    {

        $profile_data = devfolio_connectionManager::Instance()->select('Select * from wp_devfolio_profiles');

        $profile = new devfolio_gitHub_profile(
            $profile_data[0]['username'],
            $profile_data[0]['truename'],
            $profile_data[0]['avatar_url'],
            $profile_data[0]['site'],
            $profile_data[0]['location'],
            $profile_data[0]['creation_date'],
            $profile_data[0]['description']

        );

        return $profile;

    }


    /**
     * @return devfolio_baseProject[]
     */
    public function LoadGitHubProjects()
    {

        $project_list = array();

        $rawlist = devfolio_connectionManager::Instance()->select('Select * from wp_devfolio_github_projects');

        foreach ($rawlist as $project_data) {


            $project = new devfolio_baseProject(

                $project_data['id'],
                $project_data['name'],
                $project_data['fullname'],
                $project_data['description'],
                $project_data['html_url'],
                $project_data['private'],
                $project_data['created_at'],
                $project_data['size'],
                $project_data['nbforks'],
                $project_data['default_branch'],
                $project_data['download_url'],
                $project_data['readme'],
                $this->LoadLanguages($project_data['id']),
                $this->LoadCommits($project_data['id']),
                '',
                ''


            );

            $project_list[] = $project;

        }

        return $project_list;


    }


    /**
     * @return devfolio_baseProject
     */
    public function LoadProject($project_id)
    {

        $project_data = devfolio_connectionManager::Instance()->select('Select * from wp_devfolio_github_projects where wp_devfolio_github_projects.id=' . $project_id);
		$license_data = devfolio_connectionManager::Instance()->select('Select * from wp_devfolio_license where wp_devfolio_license.project_id=' . $project_id);
		
        $project = new devfolio_baseProject(

            $project_data[0]['id'],
            $project_data[0]['name'],
            $project_data[0]['fullname'],
            $project_data[0]['description'],
            $project_data[0]['html_url'],
            $project_data[0]['private'],
            $project_data[0]['created_at'],
            $project_data[0]['size'],
            $project_data[0]['nbforks'],
            $project_data[0]['default_branch'],
            $project_data[0]['download_url'],
            $project_data[0]['readme'],
            $this->LoadLanguages($project_data[0]['id']),
            $this->LoadCommits($project_data[0]['id']),
			$license_data[0]['name'],
			$license_data[0]['url']


        );

        return $project;

    }


    /**
     * @param $project_id
     * @return devfolio_language[]
     */
    private function LoadLanguages($project_id)
    {

        $languages = array();

        $rawlist = devfolio_connectionManager::Instance()->select('Select * from wp_devfolio_languages where wp_devfolio_languages.project_id=' . $project_id . ' order by nb_bytes desc');

        foreach ($rawlist as $language_data) {

            $language = new devfolio_language(

                $language_data['id'],
                $language_data['name'],
                $language_data['nb_bytes'],
                $language_data['percent']


            );

            $languages[] = $language;

        }

        return $languages;

    }

    /**
     * @param $project_id
     * @return devfolio_commit[]
     */
    private function LoadCommits($project_id)
    {

        $commits = array();

        $rawlist = devfolio_connectionManager::Instance()->select('Select * from wp_devfolio_commits where wp_devfolio_commits.project_id=' . $project_id);

        foreach ($rawlist as $commit_data) {

            $commit = new devfolio_commit(

                $commit_data['id'],
                $commit_data['message'],
                $commit_data['author'],
                $commit_data['date'],
                $commit_data['html_url']


            );

            $commits[] = $commit;

        }


        return $commits;


    }


}


?>