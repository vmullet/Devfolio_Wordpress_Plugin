<?php
/**
 * Created by PhpStorm.
 * User: Valentin
 * Date: 14/01/2017
 * Time: 20:55
 */

include_once('devfolio_queryManager.class.php');


class devfolio_tokenManager  {


    private static $instance = null;
    public $options;


    private function __construct()
    {

        $this->options = get_option('devfolio_options');
    }

    public static function Instance() {

        if (!isset(self::$instance)) {

            self::$instance = new devfolio_tokenManager();

        }

        return self::$instance;

    }


    public function isBitBucketTokenValid() {

        if ($this->options['devfolio_bitbucket_token']=='') {
            return false;
        }
        else {
            $response_code = devfolio_queryManager::Instance()->getStatusCode('https://api.bitbucket.org/2.0/users/'.$this->options['devfolio_bitbucket_username'].'?access_token='.$this->options['devfolio_bitbucket_token']);

            if ($response_code != '200')
                return false;
            else
                return true;
        }

    }


    public function refresh_bitbucket_token() {

        $args = array(
            'headers' => array(
                'content-Type' => 'application/x-www-form-urlencoded'
            ),
            'body' => array(
                'grant_type' => 'refresh_token',
                'client_id' => $this->options['devfolio_bitbucket_client_id'],
                'client_secret' => $this->options['devfolio_bitbucket_client_secret'],
                'refresh_token' => $this->options['devfolio_bitbucket_refresh_token']

            )

        );

        $response = devfolio_queryManager::Instance()->post('https://bitbucket.org/site/oauth2/access_token',$args);

        $this->options['devfolio_bitbucket_token'] = $response['access_token'];
        update_option('devfolio_options',$this->options);

    }


}


?>