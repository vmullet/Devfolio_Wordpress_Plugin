<?php

require_once('devfolio_optionManager.class.php');

class devfolio_pluginManager {


    protected static $instance = null;

    const DEVFOLIO_CACHE = 'DevFolio Cache';
    const DEVFOLIO_SCAN_CONTENT = 'DevFolio Scan Content';

    private function __construct()
    {
    }

    public static function Instance()
    {

        if (!(isset(self::$instance))) {

            self::$instance = new devfolio_pluginManager();
        }

        return self::$instance;

    }

    public function create_options() {

       devfolio_optionManager::Instance()->init_options();

    }

    public function remove_options() {

       devfolio_optionManager::Instance()->erase_options();

    }

    public function create_tables() {

        $project_table  = file_get_contents(dirname(__FILE__,2) . '/sql/wp_devfolio_github_projects.sql');
        $commit_table   = file_get_contents(dirname(__FILE__,2) . '/sql/wp_devfolio_commits.sql');
        $profile_table  = file_get_contents(dirname(__FILE__,2) . '/sql/wp_devfolio_profiles.sql');
        $content_table  = file_get_contents(dirname(__FILE__,2) . '/sql/wp_devfolio_content.sql');
        $language_table = file_get_contents(dirname(__FILE__,2) . '/sql/wp_devfolio_languages.sql');
        $license_table  = file_get_contents(dirname(__FILE__,2) . '/sql/wp_devfolio_license.sql');

        $commit_table   = explode(';', $commit_table);
        $content_table  = explode(';', $content_table);
        $language_table = explode(';', $language_table);


        devfolio_connectionManager::Instance()->query($project_table);
        devfolio_connectionManager::Instance()->query($profile_table);
        devfolio_connectionManager::Instance()->query($license_table);

        foreach ($commit_table as $query) {
            devfolio_connectionManager::Instance()->query($query);
        }

        foreach ($content_table as $query) {
            devfolio_connectionManager::Instance()->query($query);
        }

        foreach ($language_table as $query) {
            devfolio_connectionManager::Instance()->query($query);
        }


    }

    public function remove_tables() {

        devfolio_connectionManager::Instance()->query('DROP TABLE IF EXISTS wp_devfolio_commits');
        devfolio_connectionManager::Instance()->query('DROP TABLE IF EXISTS wp_devfolio_content');
        devfolio_connectionManager::Instance()->query('DROP TABLE IF EXISTS wp_devfolio_languages');
        devfolio_connectionManager::Instance()->query('DROP TABLE IF EXISTS wp_devfolio_license');
        devfolio_connectionManager::Instance()->query('DROP TABLE IF EXISTS wp_devfolio_profiles');
        devfolio_connectionManager::Instance()->query('DROP TABLE IF EXISTS wp_devfolio_github_projects');


    }

    public function create_pages() {

        /////////////////// Create DevFolio Cache Page ////////////////////////

        $cache_page_info = array(
            'comment_status' => 'closed',
            'ping_status' => 'closed',
            'post_title' => self::DEVFOLIO_CACHE,
            'post_author' => '1',
            'post_status' => 'publish',
            'post_type' => 'page',
            'post_name' => 'devfolio-cache',
            'page_template' => 'template-devfolio-cache.php'
        );

        if (null == get_page_by_title(self::DEVFOLIO_CACHE)) {
            wp_insert_post($cache_page_info);

        }

        /////////////////// Create Scan Explorer Page ////////////////////////

        $explorer_page_info = array(
            'comment_status' => 'closed',
            'ping_status' => 'closed',
            'post_title' => self::DEVFOLIO_SCAN_CONTENT,
            'post_author' => '1',
            'post_status' => 'publish',
            'post_type' => 'page',
            'post_name' => 'devfolio-scan-content',
            'page_template' => 'template-devfolio-scan.php'
        );

        if (null == get_page_by_title(self::DEVFOLIO_SCAN_CONTENT)) {
            wp_insert_post($explorer_page_info);
        }


    }

    public function remove_pages() {

        $cache_page = get_page_by_title(self::DEVFOLIO_CACHE);
        $scan_page  = get_page_by_title(self::DEVFOLIO_SCAN_CONTENT);

        if (null != $cache_page) {
            wp_delete_post($cache_page->ID, true);
        }
        if (null != $scan_page) {
            wp_delete_post($scan_page->ID, true);
        }

    }

    public function create_files() {

        /////////////////// Copy Template php Pages ///////////////////////////

        if (!file_exists(get_template_directory() . '/template-devfolio-cache.php')) {
            copy(dirname(__FILE__,2) . '/templates/template-devfolio-cache.php', get_template_directory() . '/template-devfolio-cache.php');
        }

        if (!file_exists(get_template_directory() . '/template-devfolio-scan.php')) {
            copy(dirname(__FILE__,2) . '/templates/template-devfolio-scan.php', get_template_directory() . '/template-devfolio-scan.php');
        }

        ///////////////// Copy Assets Folder ///////////////////////////////

        //exec("cp -r ".dirname(__FILE__)."/assets ".get_template_directory().'/assets');
        $path = dirname(__FILE__,2) . '/assets';
        mkdir(get_template_directory() . '/assets');
        $directory_main = opendir($path);
        // Read and copy recursively 'assets' folder
        $this->read_recursively($directory_main, $path);

    }

    public function remove_files() {

        if (file_exists(get_template_directory() . '/template-devfolio-cache.php')) {
            unlink(get_template_directory() . '/template-devfolio-cache.php');
        }

        if (file_exists(get_template_directory() . '/template-devfolio-cache.php')) {
            unlink(get_template_directory() . '/template-devfolio-scan.php');
        }

        // Remove assets folder recursively
        $this->delete_files_recursive(get_template_directory() . '/assets');

    }


    private function read_recursively($source, $name)
    {

        while (($file = readdir($source)) != false) {
            if ($file != '.' && $file != '..') {

                if (is_dir($name . '/' . $file)) {

                    if (!file_exists(get_template_directory() . '/assets/' . $file)) {
                        mkdir(get_template_directory() . '/assets/' . $file);
                    }

                    $open = opendir($name . '/' . $file);
                    $this->read_recursively($open, $name . '/' . $file);

                }

                else {
                    if (file_exists(get_template_directory() . '/assets/' . basename($name) . '/' . $file)) {
                        unlink(get_template_directory() . '/assets/' . basename($name) . '/' . $file);
                    }
                    copy($name . '/' . $file, get_template_directory() . '/assets/' . basename($name) . '/' . $file);
                }


            }


        }

    }

    private function delete_files_recursive($target)
    {
        if (is_dir($target)) {
            $files = glob($target . '*', GLOB_MARK); //GLOB_MARK adds a slash to directories returned

            foreach ($files as $file) {
                $this->delete_files_recursive($file);
            }

            rmdir($target);
        } elseif (is_file($target)) {
            unlink($target);
        }
    }



}



?>